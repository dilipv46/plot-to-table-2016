#! /usr/bin/env python

from glob import glob
import os
from subprocess import call

call(['sudo', 'add-apt-repository', 'universe'])
call(['sudo', '-E', 'apt-get', 'update'])

os.chdir("debs")

for directory in os.listdir('.'):
	os.chdir(directory)
	call(['bash', 'install.sh'])
	os.chdir('..')

os.chdir("..")

call(['sudo', 'apt-get', 'install', 'pdftk'])
call(['sudo', 'python', 'setup.py', 'install'])